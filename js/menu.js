const menuBtn = document.querySelector('.btn-menu');
        const menu = document.querySelector('.menu');
        const body = document.querySelector('body');
        
        menuBtn.addEventListener('click', openNav);
  
        function openNav(){
         if(body.classList.contains("closeNav") === true){
            body.classList.toggle('closeNav');
            menu.classList.toggle('close');
            menu.classList.toggle('open');
            menuBtn.classList.toggle('openBtn');
         } else {
            body.classList.toggle('closeNav');
            menu.classList.toggle('open');
            menu.classList.toggle('close');
            menuBtn.classList.toggle('openBtn');
         }
        }